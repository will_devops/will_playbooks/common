# Changelog

## 1.0.0

### Features

* Add support to network customization.

* Add hostname customization.

* Change ntp role and add tag ntp.

### Fixes

* Fix wrong comment main.yml.